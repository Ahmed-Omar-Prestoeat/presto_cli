import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: Container, initial: true),
])
class $AppRouter {}
